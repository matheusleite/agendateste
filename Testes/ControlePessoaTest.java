package Testes;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import agendaoo.controle.ControlePessoa;
import agendaoo.modelo.Pessoa;

public class ControlePessoaTest {

	private Pessoa pessoaTest;
	private ControlePessoa controleTest;
	
	@Before
	public void setUp() throws Exception{
		
	}

	@Test
	public void testNome() {
		pessoaTest = new Pessoa();
		pessoaTest.setNome("Matheus");
		controleTest = new ControlePessoa();
		controleTest.adicionar(pessoaTest);
		
		
		assertEquals(controleTest.adicionar(pessoaTest), "Pessoa adicionada com Sucesso!");
	}

}
